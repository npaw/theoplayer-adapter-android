package com.npaw.youbora.lib6.theoplayer

import com.npaw.youbora.lib6.YouboraLog
import com.npaw.youbora.lib6.YouboraUtil
import com.npaw.youbora.lib6.adapter.PlayerAdapter

import com.theoplayer.android.api.THEOplayerView
import com.theoplayer.android.api.event.EventListener
import com.theoplayer.android.api.event.ads.AdBreakBeginEvent
import com.theoplayer.android.api.event.ads.AdBreakEndEvent
import com.theoplayer.android.api.event.ads.AdsEventTypes
import com.theoplayer.android.api.event.player.*

import java.util.*

class TheoplayerAdapter(player: THEOplayerView) : PlayerAdapter<THEOplayerView>(player) {

    private val playerObject = player.player

    private var lastWidth = 0
    private var lastHeight = 0
    private var isOnABreak = false
    private var lastPlayhead = super.getPlayhead()
    private var lastLatency = super.getLatency()

    private val playEvent = EventListener<PlayEvent> {
        if (!isOnABreak) {
            fireResume()
            fireStart()
        }
    }

    private val pauseEvent = EventListener<PauseEvent> {
        if (!isOnABreak) firePause()
    }
    private val playingEvent = EventListener<PlayingEvent> {
        if (!isOnABreak) {
            fireStart()
            fireJoin()
        }
    }

    private val seekingEvent = EventListener<SeekingEvent> {
        fireSeekBegin()
    }
    private val seekedEvent = EventListener<SeekedEvent> {
        fireSeekEnd()
    }
    private val endedEvent = EventListener<EndedEvent> {
        fireStop()
    }
    private val waitingEvent = EventListener<WaitingEvent> {
        fireBufferBegin()
    }
    private val canPlayEvent = EventListener<CanPlayEvent> {
        fireBufferEnd()
    }
    private val errorEvent = EventListener<ErrorEvent> {
            errorEvent ->
        errorEvent.errorObject.message?.takeIf {
            it.contains("document could not be loaded - Invalid XML")
        }?.let { fireError(it) } ?: fireFatalError(errorEvent.errorObject.message)
    }

    override fun fireBufferEnd(params: MutableMap<String, String>) {
        if (flags.isSeeking){
            super.fireBufferEnd(params)
        } else if(chronos.buffer.getDeltaTime() > 500) {
            super.fireBufferEnd(params)
        } else {
            flags.isBuffering = false
            chronos.buffer.stop()
            YouboraLog.notice("Buffer Ignored")
        }
    }

    private val adBreakBegin = EventListener<AdBreakBeginEvent> { isOnABreak = true }
    private val adBreakEnd = EventListener<AdBreakEndEvent> { isOnABreak = false }

    init { registerListeners() }

    override fun registerListeners() {
        super.registerListeners()

        playerObject.addEventListener(PlayerEventTypes.PLAY, playEvent)
        playerObject.addEventListener(PlayerEventTypes.PAUSE, pauseEvent)
        playerObject.addEventListener(PlayerEventTypes.PLAYING, playingEvent)
        playerObject.addEventListener(PlayerEventTypes.SEEKING, seekingEvent)
        playerObject.addEventListener(PlayerEventTypes.SEEKED, seekedEvent)
        playerObject.addEventListener(PlayerEventTypes.ENDED, endedEvent)
        playerObject.addEventListener(PlayerEventTypes.WAITING, waitingEvent)
        playerObject.addEventListener(PlayerEventTypes.CANPLAY, canPlayEvent)
        playerObject.addEventListener(PlayerEventTypes.ERROR, errorEvent)
        playerObject.ads.addEventListener(AdsEventTypes.AD_BREAK_BEGIN, adBreakBegin)
        playerObject.ads.addEventListener(AdsEventTypes.AD_BREAK_END, adBreakEnd)
    }

    override fun unregisterListeners() {
        super.unregisterListeners()

        playerObject.removeEventListener(PlayerEventTypes.PLAY, playEvent)
        playerObject.removeEventListener(PlayerEventTypes.PAUSE, pauseEvent)
        playerObject.removeEventListener(PlayerEventTypes.PLAYING, playingEvent)
        playerObject.removeEventListener(PlayerEventTypes.SEEKING, seekingEvent)
        playerObject.removeEventListener(PlayerEventTypes.SEEKED, seekedEvent)
        playerObject.removeEventListener(PlayerEventTypes.ENDED, endedEvent)
        playerObject.removeEventListener(PlayerEventTypes.WAITING, waitingEvent)
        playerObject.removeEventListener(PlayerEventTypes.CANPLAY, canPlayEvent)
        playerObject.removeEventListener(PlayerEventTypes.ERROR, errorEvent)
        playerObject.ads.removeEventListener(AdsEventTypes.AD_BREAK_BEGIN, adBreakBegin)
        playerObject.ads.removeEventListener(AdsEventTypes.AD_BREAK_END, adBreakEnd)
    }

    override fun getBitrate(): Long? {
        val videoTracks = playerObject.videoTracks
        var bandwidth = super.getBitrate()

        if (videoTracks.length() > 0)
            videoTracks.getItem(0).activeQuality?.let { bandwidth = it.bandwidth }

        return bandwidth
    }

    override fun getPlayrate(): Double {
        return if (playerObject.playbackRate == 0.0) super.getPlayrate()
        else playerObject.playbackRate
    }

    override fun getPlayhead(): Double? {
        if (!isOnABreak) playerObject.requestCurrentTime { lastPlayhead = it }

        return lastPlayhead
    }

    override fun getRendition(): String {
        playerObject.requestVideoWidth { lastWidth = it }
        playerObject.requestVideoHeight { lastHeight = it }

        var bandwidth = 0.0
        getBitrate()?.let { bandwidth = it.toDouble() }

        return YouboraUtil.buildRenditionString(lastWidth, lastHeight, bandwidth)
    }

    override fun getLatency(): Double? {
        playerObject.requestCurrentProgramDateTime { date ->
            date?.let {
                lastLatency = (Date().time - it.time).toDouble()
                YouboraLog.debug("Latency: $lastLatency")
            }
        }

        return lastLatency
    }

    override fun getDuration(): Double { return playerObject.duration }
    override fun getResource(): String? { return playerObject.src }
    override fun getPlayerVersion(): String? { return player?.version }
    override fun getPlayerName(): String { return "THEOplayer" }
    override fun getVersion(): String { return BuildConfig.VERSION_NAME + "-" + getPlayerName() }

    override fun fireStop(params: MutableMap<String, String>) {
        super.fireStop(params)
        resetValues()
    }

    private fun resetValues() {
        lastHeight = 0
        lastWidth = 0
        isOnABreak = false
        lastPlayhead = super.getPlayhead()
        lastLatency = super.getLatency()
    }
}