package com.npaw.youbora.lib6.theoplayer

import com.npaw.youbora.lib6.adapter.AdAdapter
import com.theoplayer.android.api.event.EventListener
import com.theoplayer.android.api.event.ads.*
import com.theoplayer.android.api.event.player.PauseEvent
import com.theoplayer.android.api.event.player.PlayEvent
import com.theoplayer.android.api.event.player.PlayerEventTypes
import com.theoplayer.android.api.event.player.PlayingEvent
import com.theoplayer.android.api.player.Player

class TheoplayerAdsAdapter(player: Player) : AdAdapter<Player>(player) {

    private val pauseEvent = EventListener<PauseEvent> { firePause() }
    private val playEvent = EventListener<PlayEvent> { fireResume() }
    private val playingEvent = EventListener<PlayingEvent> { fireJoin() }
    private val adBreakEndEvent = EventListener<AdBreakEndEvent> { fireAdBreakStop() }
    private val adBeginEvent = EventListener<AdBeginEvent> { fireStart() }
    private val adEndEvent = EventListener<AdEndEvent> { fireStop() }
    private val adErrorEvent = EventListener<AdErrorEvent> {
        fireFatalError(null, it.error, null)
    }

    private var lastPlayhead = super.getPlayhead()

    init { registerListeners() }

    override fun registerListeners() {
        super.registerListeners()

        player?.addEventListener(PlayerEventTypes.PAUSE, pauseEvent)
        player?.addEventListener(PlayerEventTypes.PLAY, playEvent)
        player?.addEventListener(PlayerEventTypes.PLAYING, playingEvent)
        player?.ads?.addEventListener(AdsEventTypes.AD_BEGIN, adBeginEvent)
        player?.ads?.addEventListener(AdsEventTypes.AD_END, adEndEvent)
        player?.ads?.addEventListener(AdsEventTypes.AD_BREAK_END, adBreakEndEvent)
        player?.ads?.addEventListener(AdsEventTypes.AD_ERROR, adErrorEvent)
    }

    override fun unregisterListeners() {
        super.unregisterListeners()

        player?.removeEventListener(PlayerEventTypes.PAUSE, pauseEvent)
        player?.removeEventListener(PlayerEventTypes.PLAY, playEvent)
        player?.removeEventListener(PlayerEventTypes.PLAYING, playingEvent)
        player?.ads?.removeEventListener(AdsEventTypes.AD_BEGIN, adBeginEvent)
        player?.ads?.removeEventListener(AdsEventTypes.AD_END, adEndEvent)
        player?.ads?.removeEventListener(AdsEventTypes.AD_BREAK_END, adBreakEndEvent)
        player?.ads?.removeEventListener(AdsEventTypes.AD_ERROR, adErrorEvent)
    }

    override fun getPlayhead(): Double? {
        player?.requestCurrentTime { lastPlayhead = it }
        return lastPlayhead
    }

    override fun fireStop(params: MutableMap<String, String>) {
        super.fireStop(params)
        resetValues()
    }

    private fun resetValues() { lastPlayhead = super.getPlayhead() }

    override fun getPlayerName(): String { return "THEOplayer" }
    override fun getVersion(): String { return BuildConfig.VERSION_NAME + "-AdsAdapter-" + getPlayerName() }
}