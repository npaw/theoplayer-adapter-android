## [6.8.4] - 2022-08-17
### Added
- Support for `theoplayer-sdk-android:basic-minapi16:3.7.0`.
### Modified
- Update plugin lib to 6.8.0.
- Change package name to `theoplayer-basic-adapter`.

## [6.8.3] - 2022-01-12
### Modified
- Update plugin lib to 6.7.56.
- Downgrade Kotlin version to 1.4.32, the same as the plugin lib.

## [6.8.2] - 2022-01-11
### Modified
- Add Ads Adapter back for TheoPlayer 2.92.0.

## [6.8.1] - 2022-01-04
### Added
- THEOplayer version 2.92.0 support.
- Add Player's activity liveCycle handling and seek feature to demo.
### Fixed
- Buffer and seek.
### Modified
- Update plugin lib to 6.7.55.
- Manage THEOplayer dependency using maven inestead of .ARR file.

## [6.8.0] - 2021-07-19
### Added
- THEOplayer version 2.86 support.

## [6.7.4] - 2021-04-13
### Modified
- Deployment platform, moved from Bintray to JFrog.

## [6.7.3] - 2021-02-22
### Added
- Support added for THEOplayer SDK version 2.81.0.

## [6.7.2] - 2020-07-31
### Fixed
- [...] document could not be loaded - Invalid XML error spamming views.

## [6.7.1] - 2020-05-12
### Fixed
- Buffer and seek.

## [6.7.0] - 2020-03-12
### Fixed
- joinTime not working when playing a pre roll.

## [6.5.0] - 2019-10-18
### Added
- Ads rework.

## [6.4.1] - 2019-05-23
### Added
- Youboralib version updated to version 6.4.4.

## [6.4.0] - 2019-04-16
### Added
- Youboralib version updated to version 6.4.0.

## [6.3.0] - 2019-01-02
### Added
- Youboralib version updated to version 6.3.3.

## [6.2.1] - 2018-10-31
### Fixed
- Crash trying to get bitrate when connection is slow.

## [6.2.0] - 2018-10-24
### Added
- Infinity support.
### Fixed
- adJoin fixed.
- Bitrate is now retrieved correctly.

## [6.0.0] - 2018-09-05
### Added
 - Release version.
