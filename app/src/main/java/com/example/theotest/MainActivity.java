package com.example.theotest;

import androidx.appcompat.app.AppCompatActivity;

import com.npaw.youbora.lib6.YouboraLog;
import com.npaw.youbora.lib6.plugin.Options;
import com.npaw.youbora.lib6.plugin.Plugin;
import com.npaw.youbora.lib6.theoplayer.TheoplayerAdapter;

import com.npaw.youbora.lib6.theoplayer.TheoplayerAdsAdapter;
import com.npaw.youbora.lib6.utils.youboraconfigutils.YouboraConfigManager;
import com.theoplayer.android.api.THEOplayerView;
import com.theoplayer.android.api.source.SourceDescription;
import com.theoplayer.android.api.source.SourceType;
import com.theoplayer.android.api.source.TypedSource;
import com.theoplayer.android.api.event.player.PauseEvent;
import com.theoplayer.android.api.event.EventListener;
import com.theoplayer.android.api.event.player.PlayEvent;
import com.theoplayer.android.api.event.player.PlayerEventTypes;
import com.theoplayer.android.api.event.player.TimeUpdateEvent;
import com.theoplayer.android.api.source.addescription.AdDescription;
import com.theoplayer.android.api.source.addescription.THEOplayerAdDescription;

import android.widget.Button;
import android.view.View;
import android.widget.TextView;
import android.os.Bundle;

public class MainActivity extends AppCompatActivity {
    THEOplayerView theoPlayerView;
    Plugin youboraPlugin;
    Button btnPlayPause;
    Button btnSkipForward;
    Button btnSkipBackward;
    TextView txtPlayStatus, txtTimeUpdate;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        theoPlayerView = findViewById(R.id.theoplayer);

        TypedSource.Builder typedSource = TypedSource.Builder
                .typedSource("https://cdn.theoplayer.com/video/elephants-dream/playlist.m3u8");
        SourceDescription.Builder sourceDescription = SourceDescription.Builder
                .sourceDescription(typedSource.build());

        // defaults ads should be inserted here: https://docs.theoplayer.com/how-to-guides/01-ads/03-how-to-set-up-vast-and-vmap.md#how-to-set-up-vast-and-vmap-ads

        theoPlayerView.getPlayer().setSource(sourceDescription.build());

        btnPlayPause = findViewById(R.id.btn_playpause);
        btnPlayPause.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (theoPlayerView.getPlayer().isPaused()) {
                    theoPlayerView.getPlayer().play();
                } else {
                    theoPlayerView.getPlayer().pause();
                }
            }
        });

        btnSkipForward = findViewById(R.id.skipForwardButton);
        btnSkipForward.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(!theoPlayerView.getPlayer().isSeeking()){
                    theoPlayerView.getPlayer().requestCurrentTime(currentTime -> {
                        double skipForwardInSeconds = currentTime + 10;
                        theoPlayerView.getPlayer().setCurrentTime(skipForwardInSeconds);
                        }
                    );
                }
            }
        });

        btnSkipBackward = findViewById(R.id.skipBackwardButton);
        btnSkipBackward.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(!theoPlayerView.getPlayer().isSeeking()){
                    theoPlayerView.getPlayer().requestCurrentTime(currentTime-> {
                                double skipBackwardInSeconds = (currentTime - 10);
                                theoPlayerView.getPlayer().setCurrentTime(skipBackwardInSeconds);
                            }
                    );
                }
            }
        });

        txtPlayStatus = findViewById(R.id.txt_playstatus);
        txtTimeUpdate = findViewById(R.id.txt_timeupdate);

        theoPlayerView.getPlayer().addEventListener(PlayerEventTypes.PLAY, new EventListener<PlayEvent>() {
            @Override
            public void handleEvent(PlayEvent playEvent) {
                txtPlayStatus.setText("Playing");
            }
        });

        theoPlayerView.getPlayer().addEventListener(PlayerEventTypes.PAUSE, new EventListener<PauseEvent>() {
            @Override
            public void handleEvent(PauseEvent pauseEvent) {
                txtPlayStatus.setText("Paused");
            }
        });

        theoPlayerView.getPlayer().addEventListener(PlayerEventTypes.TIMEUPDATE, new EventListener<TimeUpdateEvent>() {
            @Override
            public void handleEvent(TimeUpdateEvent timeUpdateEvent) {
                txtTimeUpdate.setText(String.valueOf(timeUpdateEvent.getCurrentTime()));
            }
        });

        initializeYoubora();
    }

    private void initializeYoubora() {
        YouboraLog.setDebugLevel(YouboraLog.Level.VERBOSE);
        Options youboraOptions = YouboraConfigManager.Companion.getInstance().getOptions(this);
        youboraPlugin = new Plugin(youboraOptions, getApplicationContext());
        youboraPlugin.setActivity(this);
        youboraPlugin.setAdapter(new TheoplayerAdapter(theoPlayerView));
        youboraPlugin.setAdsAdapter(new TheoplayerAdsAdapter(theoPlayerView.getPlayer()));
    }

    @Override
    protected void onPause() {
        super.onPause();
        theoPlayerView.onPause();
    }

    @Override
    protected void onResume() {
        super.onResume();
        theoPlayerView.onResume();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        theoPlayerView.onDestroy();
    }
}